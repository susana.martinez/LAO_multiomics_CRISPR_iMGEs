# CRISPR-MGE pipeline 

The CRISPR-MGE pipeline identifies the CRISPRs from reads and contigs, and invasive mobile genetic elements (iMGEs), in particular bacteriophages and plasmids, from contigs. Then, CRISPR elements are assigned to representative genomes (ReGes) previously defined. Finally, iMGE-host networks are provided.

## The pipeline contains the following workflows, automated using [snakemake](https://academic.oup.com/bioinformatics/article/28/19/2520/290322):
* [CRISPR prediction](CRISPR-prediction.md)
* [iMGEs prediction](CRISPR-prediction.md)
* [iMGEs dereplication](MGE-dereplication.md)
* [iMGEs remapping](MGE-remapping.md)
* [iMGE-Hosts CRISPR-mediated links](MGE-host-link.md)

## Dependencies
- [snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)
- [CRASS](http://ctskennerton.github.io/crass/)
- [metaCRT](http://omics.informatics.indiana.edu/CRISPR/)
- [CD-HIT](http://weizhongli-lab.org/cd-hit/)
- [blast](https://blast.ncbi.nlm.nih.gov/Blast.cgi)
- [VirSorter](https://github.com/simroux/VirSorter)
- [VirFinder](https://github.com/jessieren/VirFinder)
- [PlasFlow](https://github.com/smaegol/PlasFlow)
- [cBar](http://csbl.bmb.uga.edu/~ffzhou/cBar/)
- [bwa](https://icb.med.cornell.edu/wiki/index.php/Elementolab/BWA_tutorial)
- [bedtools](https://bedtools.readthedocs.io/en/latest/) 
- [featureCounts](http://bioinf.wehi.edu.au/subread-package/)
- At least R version 3.4 and [tidyverse package](https://www.tidyverse.org/)
